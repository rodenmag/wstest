from django.db import models
from django.contrib.auth.models import User



class Genre(models.Model):
	genre_name = models.CharField(max_length=50)

class Names(models.Model):
	full_name = models.CharField(max_length=50)
	author = models.ForeignKey(User, on_delete=models.CASCADE)
	genre = models.ForeignKey(Genre, on_delete=models.CASCADE)

