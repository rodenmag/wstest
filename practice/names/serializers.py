from .models import *
from rest_framework import serializers


class GenreSerializer(serializers.ModelSerializer):
	class Meta:
		model = Genre
		fields = ('__all__')


class NamesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Names
        fields = ('full_name', 'author', 'genre')

