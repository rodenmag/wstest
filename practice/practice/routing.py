from django.conf.urls import url
from django.urls import path, re_path
from channels.routing import ProtocolTypeRouter, URLRouter
from channels.auth import AuthMiddlewareStack
from channels.security.websocket import AllowedHostsOriginValidator, OriginValidator

from names.consumers import GenreConsumer
application = ProtocolTypeRouter({
    'websocket': AllowedHostsOriginValidator(
    	AuthMiddlewareStack(
    		URLRouter ([
	    		url(r"^$", GenreConsumer),
	    	])
    	)
    )
})


#ws://domain/int
